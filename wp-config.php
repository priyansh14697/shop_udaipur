<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&?{/)2&t84R 797k-sRLWD_>!otcrT?OFyKt.*!yy^*1FaEMv,j]+dur|!O#QX}~');
define('SECURE_AUTH_KEY',  ' y-SXA8|Y 8G-*pq|MyQg6Up)q[HO;O6,@^zOA*s 9&mVhIydvkHgJYmdTo`Gf$y');
define('LOGGED_IN_KEY',    'f55g@uU4z,VMN0O)Xl!(in_y@cEbom?:j^H 0#yT$Qx>B}^rCF/ecnDeyymZQUgh');
define('NONCE_KEY',        '%e3`O4D(Ag;pllT40@3~vEz9Yt7++?kuc5O6:)<OeH5gY!.8}1pqVqg.4/2&(QO@');
define('AUTH_SALT',        '[>g_pQ/WIZ4sYlbLRHu;st@{vD^-haikA7o{Grg{f@ITir(R,FAJk_Bs)y9FIuk|');
define('SECURE_AUTH_SALT', '~1b1,~b_f8kty*%T?4(3vqMhDjgU|]`5sIE SyyhbkZ(=hGW3{o/jc5)m_}4kg1I');
define('LOGGED_IN_SALT',   '<ki2;/mIM.S4ND^45;l0va0.dlMv[fyRE%c|0/*~f,2LFz0C[s$[V+jP$YHlS9tX');
define('NONCE_SALT',       '0<Prc,`cxK.B,`Ecy|IHkG,_Q0pPg%/HzU=VBZP)bW{9hF);tbNl(e@g6~?]9ql.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
